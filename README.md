# SSI Overview

![Digicampus](https://digicampus.tech/wp-content/uploads/2020/04/logo-digicampus.svg)

![Dutch Blockchain Coalition](https://dutchblockchaincoalition.org/assets/images/Logo-DBC.png)

This repo provides a short overview of SSI-related technologies, frameworks along with links to information.
In addition, points of contact that are present for [Odyssey 2020](https://odyssey.org) are listed.

These lists are work in progress, feel free to submit a Pull Request with additional technologies or information. 

## Odyssey SSI teams

|Team|Description|Documentation|Online channel|Point of contact|
|---|---|---|---|---|
|DUO Innovatielab|||| Jan van Mulligen |
|LunarPunk Labs|||| Josh Fairhead |
|Rabobank SSI Team|||| Kim Schneider |
|Hyper42| Our service supports different types of authorizations (rule based, short term and revocable). It complies with the GDPR legislation and can be used by any type of wallet. For more info regarding connecting (including manual and contact info) please head on over to https://hyper42.nl/index.php/momentum2020!|https://hyper42.nl/index.php/momentum2020|| Ralph Verhelst |
|Conduction|||| Matthias Oliveiro |
|OdySSI team|<h2>Working with credentials made easy</h2>*The OdySSI team strives to improve the usability of SSI in real life by providing basic functionality that assists in issuing and verifying credentials to and from multiple wallets.*<br><br>Do you want to issue or verify credentials, but lack either the time, manpower, interest, or experience to implement this yourself? Or do you simply want to avoid the hastle of implementing support for multiple different wallets, and dealing with (breaking) changes on their end? Then you may want to consider our SSI service!<br><br>Similar to a payment service provider allowing webshops to easilly support payment with MasterCard, Visa, PayPal, iDeal, bitcoin, and you name it, our objective is to faciliatate issuing and verifying to and from any of the popular SSI wallets. We currently support IRMA, Jolocom, and Hyperledger Indy, with more to come.<br><br>Sounds interesting? Or have any questions? Please let us know and we'll help you get up and running with our SSI service, or discuss your needs for credentials.|[Documentation](https://gitlab.com/digicampus/ssi/ssi-service-provider/developer-docs)|| [Michiel Stornebrink](mailto:michiel.stornebrink@tno.nl?cc=peter.langenkamp@tno.nl) |
|WorkPi|||| Rik Rapmund |
|VIDchain|VIDchain is an SSI service, composed of diferent building blocks:<br>- VIDwallet, a wallet to hold Credentials (VC).<br>- VIDauth, an OpenID provider able to perform DID authentication.<br>- VIDcredentials, an API that allows creds management.<br>- DID SIOP lib, a Typescript library to help connect your app with VIDwallet|[VIDchain docs](https://validatedid.github.io/vidchain-doc)|[VIDchain's Discord](https://discod.gg/Fw5Ev6K)| Xavier Vila |
|Tykn|||| Eduardo Elias Saleh |
|Luxoft|||| Alexandr Kopnin |

## Platforms

|Platform|Description|Documentation|Online channel|Point of contact|
|---|---|---|---|---|
|[Holochain](http://holochain.com/)|Distributed Hash Table|[Storybook](https://holochain.github.io/holochain-ui/?selectedKind=HoloVault&selectedStory=Keep%20your%20data%20private&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Fnotes%2Fpanel)|[Forum](https://forum.holochain.org/)| Eric Bear |
|[HyperLedger Indy](https://www.hyperledger.org/projects/hyperledger-indy)|Distributed Ledger|[Walkthrough](https://github.com/hyperledger/indy-sdk/blob/master/docs/getting-started/indy-walkthrough.md)|[Rocket Chat](https://chat.hyperledger.org/channel/indy)|   |
|[ION](https://github.com/decentralized-identity/ion)|Distributed DID network|[Installation Guide](https://github.com/decentralized-identity/ion/blob/master/install-guide.md)|||
|[IRMA](https://privacybydesign.foundation/en/)|SSI solution|[Documentation](https://irma.app/docs/what-is-irma/)|[GitHub](https://github.com/privacybydesign)|[Bob Kronenburg](mailto:Bob.Kronenburg@sidn.nl)|
|[SSI Provider](https://service.ssi-lab.nl/)|SSI platform for issuing and verifying credentials|[Documentation](https://gitlab.com/digicampus/ssi/ssi-service-provider/developer-docs)||[Peter Langenkamp](mailto:peter.langenkamp@tno.nl?cc=michiel.stornebrink@tno.nl)|
|[Threefold](https://cloud.threefold.io)|Peer-to-peer cloud infrastructure|[Documentation](https://sdk.threefold.io/#/), Decentralised Login [JS SDK](https://github.com/threefoldtech/threefold-login-js-sdk) and [Python SDK](https://github.com/threefoldtech/threefold-login-python-sdk)|[Forum](https://forum.threefold.io/), login through [3Bot Connect](https://3bot.org/3bot.html)|[Weynand Kuijpers](https://www.linkedin.com/in/weynandkuijpers/) or [Geert Machtelinckx](https://www.linkedin.com/in/geert-machtelinckx-a72453b/)|

## Frameworks

|Framework|Description|Documentation|Online channel|Point of contact|
|---|---|---|---|---|
|[Discipl](https://discipl.org/)|Build solutions on top of SSI|[Main repo](https://github.com/discipl/main)|[Slack (see main repo for invite link)](https://discipl.slack.com)|[Bas Kaptijn](https://github.com/bkaptijn) or [Pim Otte](https://github.com/pimotte)|
|[IPv8](https://github.com/Tribler/py-ipv8)|Distributed networking library|[Read the Docs](https://py-ipv8.readthedocs.io/en/latest/)|[GitHub](https://github.com/Tribler/py-ipv8)||
|[Universal Ledger Agent](https://github.com/WebOfTrustInfo/rwot8-barcelona/blob/master/topics-and-advance-readings/universal-ledger-agent.md)|Interoperability between SSI solutions|[Main repo](https://github.com/rabobank-blockchain), also API available|[Slack invite link](https://join.slack.com/t/rabobankssi/shared_invite/zt-d58zvo4j-MjXbGJ1UrcOhcGHREQAbsA)|[Marnix van den Bent (technical)](https://www.linkedin.com/in/marnix-van-den-bent/) or [David Lamers (use cases)](https://www.linkedin.com/in/lamersdavid/)|

## Technology and tools

|Tech|Description|Documentation|Online channel|Point of contact|
|---|---|---|---|---|
|[Tangem made-for-blockchain NFC](https://www.gimly.io/blog/odyssey-tangem)|Provide physical objects with digital IDs with Tangem's made-for-blockchain NFC chips.|[Gimly Github](https://github.com/Gimly-Blockchain/Tangem-Blockchain-NFC)|[Discord](https://discord.gg/RXK992X)| [Caspar Roelofs (Gimly)](mailto:tangem@gimly.io?SUBJECT=Tangem%20for%20Odyssey%20SSI-toolbox) |
|VIDchain OpenID SSI provider|An OpenID provider that is able to perform DID authentication|[VIDchain doc](https://validatedid.github.io/vidchain-doc) <br>and [OpenID provider doc](https://validatedid.github.io/vidchain-doc/#/requirements?id=set-up-your-oidc-client)|[VIDchain's Discord](https://discod.gg/Fw5Ev6K)| [Xavier Vila (VIDchain)](mailto:xavi.vila@validated.id?SUBJECT=OpenID%20for%20Odyssey) |
|eIDAS Bridge|A component to bring legal value to Verifiable Credentials by sealing them with Qualified Electronic Certificates|[eIDAS Bridge](https://joinup.ec.europa.eu/collection/ssi-eidas-bridge) <br>and [eIDAS Bridge Github](https://github.com/validatedid/eidas-bridge)|[VIDchain's Discord](https://discod.gg/Fw5Ev6K)| [Xavier Vila (VIDchain)](mailto:xavi.vila@validated.id?SUBJECT=eIDAS%20Bridge%20for%20Odyssey) |
|DID SIOP library|A library to help connect your app with a Wallet using OIDC SIOP|[DID SIOP documentation](https://validatedid.github.io/vidchain-doc/#/did-auth) <br>and [DID SIOP lib](https://www.npmjs.com/package/@validatedid/did-auth)|[VIDchain's Discord](https://discod.gg/Fw5Ev6K)| [Xavier Vila (VIDchain)](mailto:xavi.vila@validated.id?SUBJECT=DID%20SIOP%20for%20Odyssey) |
## [Standards](./standards.md)
- [List of standards](./standards.md) provided by NEN

## Other resources
- [Data sets about companies from Chamber of Commerce](https://www.kvk.nl/advies-en-informatie/innovatie/kvk-innovatielab/) - KvK website (Dutch)
- [Overview of Decentralized Identity Standards](https://medium.com/decentralized-identity/overview-of-decentralized-identity-standards-f82efd9ab6c7) - Medium article
- SSI eIDAS report - [Flyer (pdf)](https://joinup.ec.europa.eu/sites/default/files/document/2020-04/SSI_eIDAS_legal_report_summary_flyer_0.pdf) and [Full report (pdf)](https://joinup.ec.europa.eu/sites/default/files/document/2020-04/SSI_eIDAS_legal_report_final_0.pdf)
